const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');

const app = express();
const hbs = require('express-hbs');
const path = require('path');

// Routers
const usersRouter = require('./routes/users.router.js');
const dashboardRouter = require('./routes/dashboard.router.js');

// Session init
app.use(session({ secret: 'ssshhhhh' }));


app.get('/', (req, res) => {
    res.writeHead(302, {
        Location: '/users/',
    });
    res.end();
});

app.use(bodyParser.json());

// Use `.hbs` for extensions and find partials in `views/partials`.
app.engine('hbs', hbs.express4({
    partialsDir: path.join(__dirname, '/views/partials'),
}));
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, '/views'));

// Static files init
app.use('/public', express.static(path.join(__dirname, '/static')));

// Router Settings
app.use('/users', usersRouter);
app.use('/dashboard', dashboardRouter);


// Partials
hbs.registerPartial('headerPartial', '');
hbs.registerPartial('footerPartial', '');
hbs.registerPartial('accountRoleSelectPartial', '');
hbs.registerPartial('navPartial', '');

app.listen(3000);

// Handlebars Helpers

hbs.registerHelper('ifCond', function (a, operator, b, options) {
    switch (operator) {
    case '==':
        return (a === b) ? options.fn(this) : options.inverse(this);
    case '===':
        return (a === b) ? options.fn(this) : options.inverse(this);
    case '!=':
        return (a !== b) ? options.fn(this) : options.inverse(this);
    case '!==':
        return (a !== b) ? options.fn(this) : options.inverse(this);
    case '<':
        return (a < b) ? options.fn(this) : options.inverse(this);
    case '<=':
        return (a <= b) ? options.fn(this) : options.inverse(this);
    case '>':
        return (a > b) ? options.fn(this) : options.inverse(this);
    case '>=':
        return (a >= b) ? options.fn(this) : options.inverse(this);
    case '&&':
        return (a && b) ? options.fn(this) : options.inverse(this);
    case '||':
        return (a || b) ? options.fn(this) : options.inverse(this);
    default:
        return options.inverse(this);
    }
});
