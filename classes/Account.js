class Account {
    constructor(email, password, firstName, middleName, lastName, accountRoleId) {
        this.EMail = email;
        this.Password = password;
        this.FirstName = firstName;
        this.MiddleName = middleName;
        this.LastName = lastName;
        this.AccountRoleId = accountRoleId;
    }
}

module.exports = Account;
