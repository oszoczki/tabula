const sql = require('../models/db.js');

class Course {
    constructor(courseName) {
        this.CourseName = courseName;
    }

    addCourseLeader(account) {
        sql.query('SELECT * FROM Courses WHERE CourseName = ?', this.CourseName, (err, res) => {
            if (err) {
                console.log('error: ', err);
            } else {
                console.log(res);
                sql.query('INSERT INTO CourseLeader (AccountId, CourseId) VALUES (?,?)', [account.AccountId, res[0].CourseId], (error, insertResult) => {
                    const data = {
                        result: null,
                        message: null,
                    };

                    if (error) {
                        console.log('error: ', error);
                    } else {
                        console.log(insertResult);
                        data.result = true;
                        data.message = 'Course leader successfully added!';
                        console.log(data);
                    }
                });
            }
        });
    }

    addCourseMember(account, result) {
        sql.query('INSERT INTO CourseMember (AccountId, CourseId) VALUES (?,?)', [account.AccountId, this.CourseId], (err, res) => {
            const data = {
                result: null,
                message: null,
            };

            if (err) {
                console.log('error: ', err);
                result(err, null);
            } else {
                console.log(res);
                data.result = true;
                data.message = 'Course leader successfully added!';

                result(null, data);
            }
        });
    }
}

module.exports = Course;
