const crypto = require('crypto');
const sql = require('./db.js');
const Account = require('../classes/Account.js');

Account.createAccount = (account, result) => {
    const data = {
        result: false,
        message: null,
    };
    sql.query('SELECT COUNT(EMail) as count FROM Accounts WHERE EMail = ?', account.EMail, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
        } else {
            console.log(res[0].count);
            // result(null, res);
            if (res[0].count === 1) {
                data.result = false;
                data.message = 'This E-Mail adress already exists!';
                // console.log(data);
                result(data, null);
            } else {
                sql.query('INSERT INTO Accounts (EMail, Password, FirstName, MiddleName, LastName, AccountRoleId) VALUES (?,?,?,?,?,?)', [account.EMail, crypto.createHash('md5').update(account.Password).digest('hex'), account.FirstName, account.MiddleName, account.LastName, account.AccountRoleId], (error, resIns) => {
                    if (error) {
                        console.log('error: ', error);
                        result(error, null);
                    } else {
                        console.log(resIns);
                        data.result = true;
                        data.message = 'Account successfully created!';

                        result(null, data);
                    }
                });
            }
        }
    });
};

Account.selectAllAccount = (result) => {
    sql.query('SELECT * FROM Accounts', (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
        } else {
            console.log(res);
            console.log('Successfull query');
            result(null, res);
        }
    });
};

Account.selectByEmail = (email, result) => {
    sql.query('SELECT * FROM Accounts WHERE EMail = ?', email, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
        } else {
            // console.log(res);
            // console.log("Successfull query");
            result(null, res);
        }
    });
};

module.exports = Account;
