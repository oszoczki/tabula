const sql = require('./db.js');
const Course = require('../classes/Course.js');

Course.createCourse = (course, result) => {
    const data = {
        result: false,
        message: null,
    };

    sql.query('INSERT INTO Courses (CourseName) VALUES (?)', course.CourseName, (err, res) => {
        if (err) {
            console.log('error: ', err);
            result(err, null);
        } else {
            console.log(res);
            data.result = true;
            data.message = 'Course successfully created!';

            result(null, data);
        }
    });
};

module.exports = Course;
