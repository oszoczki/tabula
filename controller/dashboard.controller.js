const DashboardModel = require('../models/dashboard.model.js');
const Course = require('../classes/Course.js');

exports.createCourse = (req, res) => {
    const course = new Course(req.body.coursename);
    const account = req.session.Account;

    DashboardModel.createCourse(course, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
            course.addCourseLeader(account);
        }
    });
};
