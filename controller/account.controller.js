const crypto = require('crypto');
const AccountModel = require('../models/account.model.js');
const Account = require('../classes/Account.js');

exports.createAccount = (req, res) => {
    const account = new Account(req.body.email, req.body.password, req.body.firstname, req.body.middlename, req.body.lastname, req.body.accountroleid);

    AccountModel.createAccount(account, (err, result) => {
        if (err) res.send(err);
        else res.send(result);
    });
};

exports.selectAllAccount = (req, res) => {
    AccountModel.selectAllAccount((err, accounts) => {
        if (err) res.send(err);
        else res.render('../views/dashboard.hbs', { accounts });
    });
};

exports.login = (req, res) => {
    const email = req.body.email;
    const password = crypto.createHash('md5').update(req.body.password).digest('hex');

    AccountModel.selectByEmail(email, (err, result) => {
        if (err) { res.send(err); }

        if (result[0].Password === password) {
            console.log('Successfull Login');
            req.session.Account = result[0];
            res.send(result);
        } else {
            console.log('Wrong Password');
        }
        console.log(result);
    });
};

exports.signOut = (req, res) => {
    delete req.session.Account;

    res.send('Sign Out');
};
