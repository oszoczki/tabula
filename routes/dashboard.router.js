const express = require('express');

const router = express.Router();
const accountController = require('../controller/account.controller.js');
const dashboardController = require('../controller/dashboard.controller.js');

router.get('/', (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.session, 'Account')) {
        res.writeHead(302, {
            Location: '/users/',
        });
        res.end();
    }
    const Account = req.session.Account;

    res.render('../views/dashboard.hbs', { role: Account.AccountRoleId });
});

router.get('/manage-accounts', (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.session, 'Account')) {
        res.writeHead(302, {
            Location: '/users/',
        });
        res.end();
    }

    const Account = req.session.Account;

    if (Account.AccountRoleId === 1) res.render('../views/manage-accounts.hbs', { role: Account.AccountRoleId });
    else {
        res.writeHead(302, {
            Location: '/dashboard/',
        });
        res.end();
    }
});

router.get('/account', (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.session, 'Account')) {
        res.writeHead(302, {
            Location: '/users/',
        });
        res.end();
    }

    const Account = req.session.Account;

    let fullName;

    if (Account.MiddleName == null) {
        fullName = `${Account.FirstName} ${Account.LastName}`;
    } else {
        fullName = `${Account.FirstName} ${Account.MiddleName} ${Account.LastName}`;
    }

    res.render('../views/account.hbs', { account: Account, fullname: fullName, role: Account.AccountRoleId });
});

router.get('/courses', (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.session, 'Account')) {
        res.writeHead(302, {
            Location: '/users/',
        });
        res.end();
    }
    const Account = req.session.Account;

    res.render('../views/courses.hbs', { role: Account.AccountRoleId });
});

router.post('/create-account', accountController.createAccount);

router.post('/create-course', dashboardController.createCourse);

module.exports = router;
