const express = require('express');

const router = express.Router();
const accountController = require('../controller/account.controller.js');

router.get('/', (req, res) => {
    if (Object.prototype.hasOwnProperty.call(req.session, 'Account')) {
        res.writeHead(302, {
            Location: '/dashboard/',
        });
        res.end();
    }

    res.render('../views/login.hbs');
});

// router.get('/create', accountController.createAccount);
router.post('/login', accountController.login);
router.post('/signout', accountController.signOut);

module.exports = router;
