jQuery(document).ready(() => {
    jQuery('#login-form').submit((e) => {
        e.preventDefault();

        jQuery.ajax({
            url: '/users/login',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                email: jQuery('#email-input').val(),
                password: jQuery('#password-input').val(),
            }),
            success: (resp) => {
                console.log('Success');
                console.log(resp);
                location.replace('/dashboard/');
            },
        });
    });

    jQuery('#sign-out-button').on('click', () => {
        jQuery.ajax({
            url: '../users/signout',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                signout: true,
            }),
            success: (resp) => {
                console.log('Success');
                console.log(resp);
                location.replace('/users/');
            },
        });
    });

    jQuery('#account-create-form').submit((e) => {
        e.preventDefault();

        jQuery.ajax({
            url: 'create-account',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                email: jQuery('#account-create-email-input').val(),
                password: jQuery('#account-create-password-input').val(),
                firstname: jQuery('#account-create-firstname-input').val(),
                middlename: jQuery('#account-create-middlename-input').val(),
                lastname: jQuery('#account-create-lastname-input').val(),
                accountroleid: jQuery('#account-create-select option:selected').data('account-role-id'),
            }),
            success: (resp) => {
                console.log(resp);
            },
        });
    });

    jQuery('nav .menu-elem').on('click', function () {
        let route = jQuery(this).data('elem');

        if (!jQuery(this).hasClass('active')) {
            jQuery('nav .menu-elem').removeClass('active');
            // jQuery(this).addClass('active');

            if (route === 'dashboard') route = `http://${window.location.hostname}:3000/dashboard/`;

            console.log(route);

            jQuery.ajax({
                url: route,
                type: 'get',
                contentType: 'application/json',
                data: '',
                success: (resp) => {
                    console.log(resp);
                    location.replace(route);
                },
            });
        }
    });

    // New Course

    jQuery('.new-course-button').on('click', () => {
        jQuery('.new-course-box').fadeIn(300);
    });

    jQuery('#new-course-form').submit((e) => {
        e.preventDefault();

        jQuery.ajax({
            url: 'create-course',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                coursename: jQuery('#course-name-input').val(),
            }),
            success: (resp) => {
                console.log(resp);
            },
        });
    });
});

// eslint-disable-next-line no-undef
particlesJS.load('particles', '../public/js/plugins/particlesjs-config.json', () => {
    console.log('callback - particles.js config loaded');
});
